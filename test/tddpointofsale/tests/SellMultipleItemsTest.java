package tddpointofsale.tests;

import java.util.Collections;
import java.util.HashMap;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Coady Duffney
 */
public class SellMultipleItemsTest {

    @Test
    public void zeroItems() {
        Display display = new Display();
        Sale sale = new Sale(null, display);

        sale.onTotal();

        Assert.assertEquals("No sale in progress. Try scanning a product.", display.getText());
    }

    @Test
    public void oneItemFound() {
        Catalog catalog = new Catalog(Collections.singletonMap("12345", 650));
        Display display = new Display();
        Sale sale = new Sale(catalog, display);

        sale.onBarcode("12345");
        sale.onTotal();

        assertEquals("Total: $6.50", display.getText());

    }

    @Test
    public void oneItemNotFound() {
        Catalog catalog = new Catalog(Collections.singletonMap("12345", 650));
        Display display = new Display();
        Sale sale = new Sale(catalog, display);

        sale.onBarcode("99999");
        sale.onTotal();

        Assert.assertEquals("No sale in progress. Try scanning a product.", display.getText());
    }

    @Test
    public void severalItemsAllNotFound() {
        Display display = new Display();
        Sale sale = new Sale(
                catalogWithoutBarcodes(
                        "product you won't find",
                        "another product you won't find",
                        "a third product you won't find"),
                display);

        sale.onBarcode("product you won't find");
        sale.onBarcode("another product you won't find");
        sale.onBarcode("a third product you won't find");
        sale.onTotal();

        Assert.assertEquals("No sale in progress. Try scanning a product.", display.getText());
    }

    @Test
    public void severalItemsAllFound() {
        Catalog catalog = new Catalog(
                new HashMap<String, Integer>() {
            {
                put("1", 850);
                put("2", 1275);
                put("3", 330);
            }
        });
        Display display = new Display();
        Sale sale = new Sale(catalog, display);

        sale.onBarcode("1");
        sale.onBarcode("2");
        sale.onBarcode("3");
        sale.onTotal();

        assertEquals("Total: $24.55", display.getText());

    }

    @Test
    public void severalItemsSomeNotFound() {
        Catalog catalog = new Catalog(
                new HashMap<String, Integer>() {
            {
                put("1", 1200);
                put("2", 500);
            }
        });
        Display display = new Display();
        Sale sale = new Sale(catalog, display);

        sale.onBarcode("1");
        sale.onBarcode("you don't know this product");
        sale.onBarcode("2");
        sale.onTotal();

        assertEquals("Total: $17.00", display.getText());
    }

    @Test
    public void severalItemsOfWhichOneIsEmpty() {
        Catalog catalog = new Catalog(
                new HashMap<String, Integer>() {
            {
                put("1", 3100);
                put("2", 460);
            }
        });
        Display display = new Display();
        Sale sale = new Sale(catalog, display);

        sale.onBarcode("1");
        sale.onBarcode("");
        sale.onBarcode("2");
        sale.onTotal();

        assertEquals("Total: $35.60", display.getText());
    }

    private Catalog emptyCatalog() {
        return new Catalog(Collections.emptyMap());
    }

    private Catalog catalogWithoutBarcodes(String... barcodesToExclude) {
        return emptyCatalog();
    }
}
