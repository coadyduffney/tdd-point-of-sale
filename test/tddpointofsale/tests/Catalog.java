package tddpointofsale.tests;

import java.util.Map;

/**
 *
 * @author Coady Duffney
 */
public class Catalog {
    
    private Map<String, Integer> pricesInCentsByBarcode;

    public Catalog(Map<String, Integer> pricesInCentsByBarcode) {
        this.pricesInCentsByBarcode = pricesInCentsByBarcode;
    }

    public String findPriceThenFormatPrice(String barcode) {
        
        Integer priceInCents = findPrice(barcode);
        
        if (priceInCents == null) {
            return null;
        } else {
            return Display.formatMonetaryAmount(priceInCents);
        }        
    }

    public Integer findPrice(String barcode) {
        return pricesInCentsByBarcode.get(barcode);
    }
    
}
