package tddpointofsale.tests;

/**
 *
 * @author Coady Duffney
 */
public class Display {

    public static String formatMonetaryAmount(int priceInCents) {
        return String.format("$%,.2f", priceInCents / 100.0);
    }
    
    private String text;

    public String getText() {
        return text;
    }

    public void displayProductNotFoundMessage(String barcode) {
        this.text = "Product not found for " + barcode;
    }

    public void displayEmptyBarcodeMessage() {
        this.text = "Scanning error: empty barcode";
    }

    public void displayNullBarcodeMessage(Sale sale) {
        this.text = "Scanning error: barcode value was null";
    }

    public void displayNoSaleInProgressMessage() {
        this.text = "No sale in progress. Try scanning a product.";
    }

    public void displayPurchaseTotal(Integer purchaseTotal) {
        this.text = "Total: " + formatMonetaryAmount(purchaseTotal);
    }

    public void displayPrice(Integer priceInCents) {
        this.text = formatMonetaryAmount(priceInCents);
    }
    
}
