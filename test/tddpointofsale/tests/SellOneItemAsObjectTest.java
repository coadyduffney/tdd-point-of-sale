package tddpointofsale.tests;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Coady Duffney
 */
public class SellOneItemAsObjectTest {

    private Display display;
    private Sale sale;
    private Barcode barcode1;
    private Barcode barcode2;
    private Price price1;
    private Price price2;

    public SellOneItemAsObjectTest() {
    }

    @Before
    public void setUp() {
        barcode1 = new Barcode("12345");
        barcode2 = new Barcode("23456");
        
        price1 = new Price("$7.95");
        price2 = new Price("$12.50");
        
        display = new Display();
        sale = new Sale(new Catalog(new HashMap<Barcode, Price>() {
            {
                put(barcode1, price1);
                put(barcode2, price2);
            }
        }), display);
    }

    @Test
    public void productFound() {
        sale.onBarcode(new Barcode("12345"));
        assertEquals("$7.95", display.getText());
    }

    @Test
    public void anotherProductFound() {
        sale.onBarcode(new Barcode("23456"));
        assertEquals("$12.50", display.getText());
    }

    @Test
    public void productNotFound() {
        sale.onBarcode(new Barcode("99999"));
        assertEquals("Product not found for 99999", display.getText());
    }

    @Test
    public void emptyBarcode() {
        sale.onBarcode(new Barcode(""));
        assertEquals("Scanning error: empty barcode", display.getText());
    }

    @Test
    public void nullBarcode() {
        sale.onBarcode(null);
        assertEquals("Error: barcode is null", display.getText());
    }

    public static class Display {

        private String text;

        public String getText() {
            return text;
        }

        public void displayPrice(final String priceAsText, Sale sale) {
            this.text = priceAsText;
            System.out.println(this.text);
        }

        public void displayProductNotFoundMessage(Barcode barcode, Sale sale) {
            this.text = "Product not found for " + barcode.getValue();
            System.out.println(this.text);
        }

        public void displayEmptyBarcodeMessage(Sale sale) {
            this.text = "Scanning error: empty barcode";
            System.out.println(this.text);
        }

        public void displayNullBarcodeMessage(Sale sale) {
            this.text = "Error: barcode is null";
            System.out.println(this.text);
        }
    }

    public static class Sale {

        private Catalog catalog;
        private Display display;

        public Sale(Catalog catalog, Display display) {
            this.catalog = catalog;
            this.display = display;
        }

        public void onBarcode(Barcode barcode) {
            if (barcode == null) {
                display.displayNullBarcodeMessage(this);
                return;
            }

            if ("".equals(barcode.getValue())) {
                display.displayEmptyBarcodeMessage(this);
                return;
            }

            final Price priceAsText = catalog.findPrice(barcode);

            if (priceAsText == null) {
                display.displayProductNotFoundMessage(barcode, this);
            } else {
                display.displayPrice(priceAsText.getValue(), this);
            }
        }
    }

    public class Catalog {

        private final Map<Barcode, Price> pricesByBarcode;

        private Catalog(Map<Barcode, Price> pricesByBarcode) {
            this.pricesByBarcode = pricesByBarcode;
        }

        public Price findPrice(Barcode barcode) {
            return pricesByBarcode.get(barcode);
        }
    }

    public static class Barcode {

        private String value;

        public Barcode(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 53 * hash + Objects.hashCode(this.value);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Barcode other = (Barcode) obj;
            if (!Objects.equals(this.value, other.value)) {
                return false;
            }
            return true;
        }
    }
    
    public class Price {
        private String value;

        public Price(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 97 * hash + Objects.hashCode(this.value);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Price other = (Price) obj;
            if (!Objects.equals(this.value, other.value)) {
                return false;
            }
            return true;
        }
        
    }
}
