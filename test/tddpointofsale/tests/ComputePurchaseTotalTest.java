package tddpointofsale.tests;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Coady Duffney
 */
public class ComputePurchaseTotalTest {
    
    @Test
    public void zeroItems() {
        assertEquals(0, computePurchaseTotal(Collections.<Integer>emptyList()));
    }
    
    @Test
    public void oneItem() {
        assertEquals(795, computePurchaseTotal(Collections.singletonList(795)));
    }
    
    @Test
    public void severalItems() {
        assertEquals(5450, computePurchaseTotal(Arrays.asList(2850, 1275, 1325)));
    }

    private static int computePurchaseTotal(List<Integer> purchaseItemPrices) {
        return Sale.computePurchaseTotal(purchaseItemPrices).intValue();
    }
}
