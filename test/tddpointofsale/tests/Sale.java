package tddpointofsale.tests;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Coady Duffney
 */
public class Sale {
    
    private Catalog catalog;
    private Display display;
    private Collection<Integer> pendingPurchaseItemPrices = new ArrayList<Integer>();

    public Sale(Catalog catalog, Display display) {
        this.catalog = catalog;
        this.display = display;
    }

    public void onBarcode(String barcode) {
        if ("".equals(barcode)) {
            display.displayEmptyBarcodeMessage();
            return;
        }
        
        Integer priceInCents = catalog.findPrice(barcode);
        
        if (priceInCents == null) {
            display.displayProductNotFoundMessage(barcode);
        } else {
            // REFACTOR Eventually a shopping cart?
            pendingPurchaseItemPrices.add(priceInCents);
            display.displayPrice(priceInCents);
        }
    }


    public void onTotal() {
        if (pendingPurchaseItemPrices.isEmpty()) {
            display.displayNoSaleInProgressMessage();
        } else {
            display.displayPurchaseTotal(pendingPurchaseTotal());
        }
    }

    // REFACTOR Looks like Model behavior to me
    private Integer pendingPurchaseTotal() {
        return computePurchaseTotal(pendingPurchaseItemPrices);
    }

    public static Integer computePurchaseTotal(Collection<Integer> purchaseItemPrices) {
        return purchaseItemPrices.stream().reduce(0, Integer::sum);       
    }
    
}
